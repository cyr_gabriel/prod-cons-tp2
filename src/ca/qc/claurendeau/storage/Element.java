package ca.qc.claurendeau.storage;

public class Element
{
    private Element nextElement;
    private int data;

    public void setData(int data)
    {
        this.data = data;
    }
    
    public int getData()
    {
        return this.data;
    }

	public Element getNextElement() {
		return this.nextElement;
	}

	public void setNextElement(Element nextElement) {
		this.nextElement = nextElement;
	}
    
    
}
