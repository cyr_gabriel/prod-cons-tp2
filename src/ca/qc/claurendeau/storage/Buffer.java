package ca.qc.claurendeau.storage;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer
{
	public static final int NO_ELEMENT = 0;
	public static final int ONE_ELEMENT = 1;
	
    private int capacity;
    private int numberElements;
    private Element firstElement;

    public Buffer(int capacity)
    {
        this.capacity = capacity;
        this.numberElements = NO_ELEMENT;
        this.firstElement = null;
    }

    // returns the content of the buffer in form of a string
    public String toString()
    {
        String string = "";
        Element currentElement = this.firstElement;
        
        while(null != currentElement){
        	string += currentElement.getData();
        	
        	if(null != currentElement.getNextElement()){
        		string += " ";
        	}
        	currentElement = currentElement.getNextElement();
        }
        return string;
    }

    // returns the capacity of the buffer
    public int capacity()
    {
        return this.capacity;
    }

    // returns the number of elements currently in the buffer
    public int getCurrentLoad()
    {
        return this.numberElements;
    }

    // returns true if buffer is empty, false otherwise
    public boolean isEmpty()
    {
        return 0 == this.numberElements;
    }

    // returns true if buffer is full, false otherwise
    public boolean isFull()
    {
        return this.capacity == this.numberElements;
    }

    // adds an element to the buffer
    // Throws an exception if the buffer is full
    public synchronized void addElement(Element element) throws BufferFullException
    {
       if(isFull()){
    	   throw new BufferFullException();
       }
       else if(isEmpty()){
    	   this.firstElement = element;
       }
       else{
    	   element.setNextElement(this.firstElement);
    	   this.firstElement = element;
       }
       this.numberElements++;
    }
    
    // removes an element and returns it
    // Throws an exception if the buffer is empty
    public synchronized Element removeElement()  throws BufferEmptyException
    {
    	Element currentElement = this.firstElement;
    	Element elementRemoved = null;
    	
        if(isEmpty()){
        	throw new BufferEmptyException();
        }
        else if(ONE_ELEMENT != this.numberElements){
        	while(null != currentElement.getNextElement().getNextElement()){
        		currentElement = currentElement.getNextElement();
        	}
        	elementRemoved = currentElement.getNextElement();
        	currentElement.setNextElement(null);
        }
        else{
        	elementRemoved = currentElement;
        	this.firstElement = null;
        }
        this.numberElements--;
        
        return elementRemoved;
    }
}
