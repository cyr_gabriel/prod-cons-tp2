package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {

	public BufferFullException() {
		super("Buffer is full");
	}

}
