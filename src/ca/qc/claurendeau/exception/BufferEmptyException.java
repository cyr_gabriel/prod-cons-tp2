package ca.qc.claurendeau.exception;

public class BufferEmptyException extends Exception {

	public BufferEmptyException() {
		super("Buffer is empty");
	}

}
