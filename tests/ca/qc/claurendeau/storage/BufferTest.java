package ca.qc.claurendeau.storage;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class BufferTest {
	
	public static final int BUFFER_SIZE = 6;
	public static final int EMPTY_LOAD = 0;
	
	Buffer buffer;
	Element element;
	Element element02;
	Element element03;
	
	@Before
	public void setUp(){
		buffer = new Buffer(BUFFER_SIZE);
		element = new Element();
		element.setData(5);
		element02 = new Element();
		element02.setData(21);
		element03 = new Element();
		element03.setData(66);
	}
	
	@After
	public void tearDown(){
		buffer = null;
		element = null;
		element02 = null;
		element03 = null;
	}
	
    @Test
    public void testBuildSuccess() {
        assertTrue(true);
    }
    
    @Test
    public void testCapacity(){
    	assertEquals(BUFFER_SIZE, buffer.capacity());
    }
    
    @Test
    public void testGetCurrentLoad(){
    	assertEquals(EMPTY_LOAD, buffer.getCurrentLoad());
    }
    
    @Test
    public void testIsEmpty() throws BufferFullException{
    	assertTrue(buffer.isEmpty());
    	buffer.addElement(element);
    	assertFalse(buffer.isEmpty());
    }
    
    @Test
    public void testIsFull() throws BufferFullException{
    	assertFalse(buffer.isFull());
    	
    	for(int i = 0; i < buffer.capacity(); i++){
    		buffer.addElement(element);
    	}
    	assertTrue(buffer.isFull());
    }
    
    @Test(expected = BufferFullException.class)
    public void testAddElement() throws BufferFullException{

    	buffer.addElement(element);
    	assertEquals(1, buffer.getCurrentLoad());
    	
    	//cas exception (buffer deja plein)
    	
    	for(int i = 0; i < buffer.capacity(); i++){
    		buffer.addElement(element);
    	}
    }
    
    @Test(expected = BufferEmptyException.class)
    public void testRemoveElement() throws BufferEmptyException, BufferFullException{
    	
    	//cas un seul �l�ment dans le buffer
    	
    	buffer.addElement(element);	
    
    	int currentLoad = buffer.getCurrentLoad();	
    	Element elementRemoved = buffer.removeElement();
    	
    	assertEquals(elementRemoved.getData(), element.getData());
    	assertEquals(currentLoad-1, buffer.getCurrentLoad());
    	
    	//cas plusieurs �l�ments dans le buffer (2)
    	
    	Element element04 = new Element();
    	element04.setData(98);
    	
    	buffer.addElement(element02);
    	buffer.addElement(element03);
    	buffer.addElement(element04);
    
    	currentLoad = buffer.getCurrentLoad();	
    	elementRemoved = buffer.removeElement();
    	
    	assertEquals(elementRemoved.getData(), element02.getData());
    	assertEquals(currentLoad-1, buffer.getCurrentLoad());
    	
    	//cas exception (quand le buffer est vide)
    	
    	while(-1 != buffer.getCurrentLoad()){
    		buffer.removeElement();
    	}
    }
    
    @Test
    public void testToString() throws BufferFullException{
    	
    	assertEquals("", buffer.toString());
    	
    	buffer.addElement(element);
    	buffer.addElement(element02);
    	buffer.addElement(element03);
    	
    	assertEquals("66 21 5", buffer.toString());
    }
    
}


